
/**
 * The file contains different language implementations
 * 0 - English
 * 1 - Finnish
 * 2 - Russian
 * 3 - Swedish
 */
'use strict'


var languageChoices = ['English', 'Suomi', 'Русский', 'Svenska'];
var wantMessage = ["What do you want?", "Mitä haluat?", "Что бы вы хотели?", "Vad vill du?"];
var findCourse = ["Find course", "Löytää kurssi", "Найти курс", "Hitta kurs"];
var findExam = ["Find exam", "Etsi Tentti", "Найти экзамен", "Hitta examen"];
var findParty = ["Find party", "Etsi bileet", "Найти вечеринку", "Hitta parti"];
var findFood = ["Find food", "Etsi ruokaa", "Посмотреть обед", "Hitta mat"];
var openHours = ["Openning hours", "Aukioloajat", "Часы работы", "Öppettider"];
var menu = ["Menu", "Ruokalista", "Меню", "Meny"];
var error = ["I cannot understand you. Please say it again!", "En ymmärrä sinua. Sano se uudelleen!", 
                "Я тебя не понимаю. Повтори еще раз!", "Jag förstår dig inte. Vänligen säga det igen!"];