'use strict'


//http://exlmoto.ru/writing-telegram-bots/
//http://catethysis.ru/node-js-telegram-bot/
//https://www.npmjs.com/package/node-telegram-bot
//https://github.com/yagop/node-telegram-bot-api
//https://github.com/yagop/node-telegram-bot-api/blob/master/examples/polling.js#L38

//Including libraries
//For using telegram API
const TelegramBot = require('node-telegram-bot-api');
//var TelegramBot = require('../src/telegram');
const localization = require('./localization.js');
const token = '242676605:AAHfADT4peWC7tTWlhIPP4ZCQC2q2B2T-H0';
//var bot = new TelegramBot(token, { polling: true });
const botOptions = {
    //polling: true
    polling: {timeout: 1, interval: 100}
};
var bot = new TelegramBot(token, botOptions);

//For parsing sites
const cheerio = require('cheerio');
const request = require('request');
/* Unused imports
const htmlParser = require('htmlparser2');
const xpath = require('xpath');
const dom = require('xmldom').DOMParser;
const sp = require('scrapejs').init({
    cc: 1, // up to 2 concurrent requests
    delay: 2 * 1000 // delay 5 seconds before each request
});
*/

//Localization variables
var languageChoices = ['English', 'Suomi', 'Русский', 'Svenska'];
var wantMessage = ["What do you want?", "Mitä haluat?", "Что бы вы хотели?", "Vad vill du?"];
var findCourse = ["🎓 Find course", "🎓 Löytää kurssi", "🎓 Найти курс" , "🎓 Hitta kurs"];
//var findExam = ["✏ Find exam", "✏ Etsi Tentti", "✏ Найти экзамен", "✏ Hitta examen"];
var findParty = ["🎉 Find party", "🎉 Etsi bileet", "🎉 Найти вечеринку", "🎉 Hitta parti"];
var findFood = ["😋 Find food", "😋 Etsi ruokaa", "😋 Посмотреть обед", "😋 Hitta mat"];
var openHours = ["🕜 Opening hours", "🕜 Aukioloajat", "🕜 Часы работы", "🕜 Öppettider"];
var settings = ['🔧 Settings', '🔧 Asetukset', '🔧 Настройки', '🔧 Inställningar'];
var menu = ["📋 Menu", "📋 Ruokalista", "📋 Меню", "📋 Meny"];
var back = ['⬅ Main menu', '⬅ Päävalikko', '⬅ Главное меню', '⬅ Huvudmeny'];
var error = ["I cannot understand you. Please say it again!", "En ymmärrä sinua. Sano se uudelleen!", 
                "Я тебя не понимаю. Повтори еще раз!", "Jag förstår dig inte. Vänligen säga det igen!"];
var chooseNext = ['Choose next option', 'Valitse seuraava vaihtoehto', 'Выбери следующую опцию', 'Välj nästa alternativ'];
var chooseRestaurant = ['Choose restaurant', 'Valitse ravintola', 'Выбери столовую', 'Välj restaurang'];
var closedRest = ['Today restaurant is closed', 'Tänään ravintola on kiinni', 'Сегодня ресторан закрыт', 'Idag är restaurangen stängd'];
var inputCourseCode = ['Please, enter the course code or name', 'Ota, anna kurssin koodi tai nimi', 'Пожалуйста, введите код курса или его название', 'Vänligen ange kurskod eller namn'];
var byCode = ['By code', 'Koodilla', 'По коду', 'Med kod'];
var byName = ['By name', 'Nimeltä', 'По названию', 'Vid namn'];
var weekDaysFi = [ 'Sunnuntai', 'Maanantai', 'Tiistai', 'Keskiviikko', 'Torstai', 'Perjantai', 'Lauantai' ];
var weekDaysEn = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
//0 - Sunday; 1 - Monday; 6 - Saturday;
var weekDayToday = new Date().getDay();
var weboodi = 'https://weboodi.lut.fi/oodi/';
var urlAalef = 'http://www.aalef.fi';
var urlSaimia = 'http://www.saimia.fi/ravintolaskinnarila/fi/'
var urlSaimiaMenu = 'http://www.saimia.fi/ravintolaskinnarila/en/menus/';
var urlLaseri = 'http://aalef.fi/laseri';
var urlStud = 'http://aalef.fi/ylioppilastalo';
var language = 0;

/**
 * This is a method which introducing the bot
 */
bot.getMe().then(function (me) {
    console.log('Hello! My name is %s!', me.first_name);
    console.log('My id is %s.', me.id);
    console.log('And my username is @%s.', me.username);
});

/**
 * This is the main method to distribute all of the requests from user
 */
bot.on('text', function(msg) {
    
    var messageChatId = msg.chat.id;
    var messageText = msg.text;
    var messageDate = msg.date;
    var messageUsr = msg.from.username;
 
    //After start
    if (messageText === '/start' || messageText == '🌐') {
        bot.sendMessage(messageChatId, 'Choose language/Valitse kieli/Выбери язык/Välj språk', languageOpts());
    }
    
    //Testing purposes
    console.log(msg);
    console.log('Course pressed ' + include(findCourse, messageText));
    console.log('Food pressed ' + include(findFood, messageText));
    console.log('Party pressed ' + include(findParty, messageText));
    //console.log('Exam pressed ' + include(findExam, messageText));
    console.log('Back pressed ' + include(back, messageText));
    console.log('Menu pressed ' + include(menu, messageText))
    console.log('Open hours pressed ' + include(openHours, messageText));

    
    //Command to have keys
    if (include(languageChoices, messageText) || messageText.match(/[Kk][Ee]([Yy][Ss]|[Yy])/)) {
        language = languageChooser(messageText);   
        bot.sendMessage(messageChatId, wantMessage[language], mainOpts());
    }
    //To go to section about Timetable search
    if ( messageText === (messageText.match(/([Tt][Ii][Mm][Ee][Tt][Aa][Bb][Ll][Ee]|[Cc][Oo][Uu][Rr][Ss]([Ee]|[Ee][Ss]))/) || {}).input 
            || include(findCourse, messageText)) {
        bot.sendMessage(messageChatId, inputCourseCode[language], timetableOpts());
        //timeTableParser(messageText);
    }

    if (include(byCode, messageText)) {
        //getDepartmentPage(messageText, messageChatId);
        //Maybe I need to invent the way how to maintain user input. Find something in wrapper info
        sendMessageByBot(messageChatId, getDepartmentPage(messageText, messageChatId));
    }

    if (include(byName, messageText)) {
        sendMessageByBot(messageChatId, 'Searching by name of the course');
    }

    //To go to section with parties
    if (messageText === (messageText.match(/([Pp][Aa]([Tt]|[Rr][Tt])([Yy]|[Ii][Ee][Ss]|[Ii])|[Rr][Ee][Ll][Aa][Xx])/) || {}).input
            || include(findParty, messageText)) {
        sendMessageByBot(messageChatId, 'Party Hard!');
    }
    //To go to section - about food
    if (messageText === (messageText.match(/([Ff][Oo][Oo][Dd]|[Ee][Aa][Tt]|[Aa][Tt][Ee])/) || {}).input
            || include(findFood, messageText)) {
        bot.sendMessage(messageChatId, chooseNext[language], foodOpts());
    }
    //To go to settings sub-menu
    if (messageText === (messageText.match(/[Ss][Ee][Tt][Tt][Ii][Nn][Gg][Ss]/) || {}).input || 
                include(settings, messageText)) {
        //sendMessageByBot('Settings can be changed here');
        console.log('Settings menu');
        var setOpts = {
                reply_markup: JSON.stringify({
                    keyboard: [
                        ['🌐'],
                        [back[language]]
                    ]
                })
        };
        bot.sendMessage(messageChatId, 'Settings menu', setOpts);
    }
    //Go to section with restaurants
    if (include(menu, messageText)) {
        bot.sendMessage(messageChatId, chooseRestaurant[language], restaurantsOpts());
    }
    //
    if (include(openHours, messageText)) {
        openHoursParser(messageChatId);
    }
    //
    if (messageText === (messageText.match(/[Bb][Aa][Cc][Kk]/) || {}).input || include(back, messageText)) {
        bot.sendMessage(messageChatId, wantMessage[language], mainOpts());
    }
    //http://aalef.fi/laseri
    if (messageText === (messageText.match(/[Ll][Aa][Ss][Ee][Rr][Ii]/) || {}).input) {
        aalefLaseriMenu(messageChatId);
    }
    //http://aalef.fi/ylioppilastalo
    if (messageText === (messageText.match(/[Uu][Nn][Ii][Oo][Nn]/) || {}).input) {
        aalefStudUnionMenu(messageChatId);
    }
    //http://www.saimia.fi/ravintolaskinnarila/fi/
    if (messageText === (messageText.match(/[Ss][Aa][Ii][Mm][Ii][Aa]/) || {}).input) {
        saimiaMenu(messageChatId);
    }
});

/**
 * Variation of sendMessageByBot function
 */
function sendMessageByBot(aChatId, aMessage) {
    bot.sendMessage(aChatId, aMessage, { caption: 'I\'m a cute bot!' });
}

/**
 * This method is created for choosing language
 */
function languageChooser(message) {
    
    if (message === languageChoices[0]) {
        return 0;
    }
    if(message === languageChoices[1]) {
        return 1;
    }
    if (message === languageChoices[2]) {
        return 2;
    }
    if (message === languageChoices[3]) {
        return 3;
    }
    return 0;
}

/**
 * This function checks if the element inside the array or not. This is needed to create 
 * localization independent answers from a user.
 */
function include(arr,obj) {
    return (arr.indexOf(obj) != -1);
}

/**
 * Buttons for main menu
 */
function mainOpts() {
        return {
                reply_markup: JSON.stringify({
                    keyboard: [
                        [findCourse[language]], 
                        [findParty[language]], 
                        [findFood[language]],
                        [settings[language]]
                    ]
                })
        };
}

/**
 * Buttons for language choose.
 */
function languageOpts() {
        return {
                reply_markup: JSON.stringify({
                keyboard: [
                        [languageChoices[0]], //['English'], 
                        [languageChoices[1]], //['Suomi'],
                        [languageChoices[2]], //['Русский'],
                        [languageChoices[3]] //['Svenska']
                ]
            })
        };
}

/**
 * Buttons in menu of finding food.
 */
function foodOpts() {
        return {
                reply_markup: JSON.stringify({
                keyboard: [
                    [openHours[language]],
                    [menu[language]],
                    [back[language]]
                ]
                })
        };
}

//Some links to help:
//https://github.com/devfom/appstore-parser
//https://github.com/fb55/htmlparser2
//https://www.npmjs.com/package/htmlparser2
//https://www.npmjs.com/package/xpath
//https://github.com/request/request
//https://github.com/Localize/node-google-translate
//http://frontender.info/web-scraping-with-nodejs/

/**
 * Buttons for choosing a restaurant.
 */
function restaurantsOpts() {
    //choosing restaurants
    return {
        reply_markup: JSON.stringify({
                keyboard: [
                    ['Ravintola Skinnarila - Saimia'],
                    ['AALEF - Yliopilastalo/Student Union'],
                    ['AALEF - Laseri/Technopolis'],
                    [findFood[language]]
                ]
        })
    };
}

/**
 * Buttons for timetable search section
 */
function timetableOpts() {
        return {
            reply_markup: JSON.stringify({
                keyboard: [
                    [byCode[language]],
                    [byName[language]],
                    [back[language]]
                ]
            })
        };
}

/**
 * Method to show the open hours of the cafes
 */
function openHoursParser(chatId) {
        //With Cheerio
        //For AALEF restaurants
        request(urlAalef, function (error, response, html) {
            if (!error && response.statusCode == 200) {
                var $ = cheerio.load(html);
                sendMessageByBot(chatId, 'ℹ AALEF restaurants ℹ \n' + $("[id='dnn_ctr6316_HtmlModule_lblContent']").text().trim().toString());
            } else {
            sendMessageByBot('Error occured');
            }
        });
        //For Saimia restaurants
        if (language !== 1) {
            urlSaimia.replaceAll('//fi//', '/en/');
        }
        
        request(urlSaimia, function (error, response, html) {
            if (!error && response.statusCode == 200) {
                var $ = cheerio.load(html);
                sendMessageByBot(chatId, 'ℹ Ravintola Skinnarila ℹ \n' + $("[style='display: none;']").text().trim().toString());
            } else {
            sendMessageByBot('Error occured');
            }
        });
}

/**
 * Method for parsing Aalef Laseri Restaurant menu.
 */
function aalefLaseriMenu(chatId) {
        switch(weekDayToday) {
            case 0:
            case 6:
                sendMessageByBot(chatId, closedRest[language]); 
                break;
            default:
                request(urlLaseri, function (error, response, html) {
                if (!error && response.statusCode == 200) {
                    var $ = cheerio.load(html);
                    sendMessageByBot(chatId, 'ℹ AALEF Laseri Menu ℹ \n' + $("[id='dnn_ctr3283_ModuleContent']").text().trim().toString());
                } else {
                    sendMessageByBot('Error occured');
                }
                });
                break;
        }
}

/**
 * Method for parsing Aalef Student Union R estaurant Menu.
 */
function aalefStudUnionMenu(chatId) {
        request(urlStud, function (error, response, html) {
            if (!error && response.statusCode == 200) {
                var $ = cheerio.load(html);
                sendMessageByBot(chatId, 'ℹ AALEF Student Union Menu ℹ \n' + $("[id='dnn_ctr3617_ModuleContent']").text().trim().toString());
            } else {
            sendMessageByBot('Error occured');
            }
        });
}

/**
 * Method for parsing Saimia Ravintola menu.
 */
function saimiaMenu(chatId) {
    /*
    http://www.storminthecastle.com/2013/08/25/use-node-js-to-extract-data-from-the-web-for-fun-and-profit/
    http://dillonbuchanan.com/programming/html-scraping-in-nodejs-with-cheerio/
    http://catethysis.ru/node-js-cheerio-parser/
    */
    var weekDayName;
    if (language === 1) {
        urlSaimiaMenu = 'http://www.saimia.fi/ravintolaskinnarila/fi/ruokalistat/';
    }
    switch(weekDayToday) {
        case 0:
        case 6:
            sendMessageByBot(chatId, closedRest[language]); 
            break;
        default:
            request(urlSaimiaMenu, function (error, response, html) {
                if (!error && response.statusCode == 200) {
                    var $ = cheerio.load(html);
                    var fullMenu = [];
                    var todayMenu = [];
                    todayMenu.length = 7;
                    var weekDayIndex;
                    $('.table-striped>tbody>tr>td:first-child').each(function(i, elem) {
                        fullMenu[i] = $(this).text().trim().toString();
                        if  (language === 1) {
                            weekDayName = weekDaysFi[weekDayToday];
                        } else {
                            weekDayName = weekDaysEn[weekDayToday];
                        }
                        if (fullMenu[i] === weekDayName) {
                            weekDayIndex = i;
                        }
                    });
                    for (var l = 0; l < todayMenu.length; l++) {
                        todayMenu[l] = fullMenu[weekDayIndex+l] + '\n';
                    }
                    sendMessageByBot( chatId, 'ℹ Saimia Ravintola Menu ℹ \n' + todayMenu.toString().replaceAll(',', ''));
                } else {
                        sendMessageByBot('Error occured');
                }
            });
            break;
    }
}

/**
 * Method for parsing timetable
 */
function timeTableParser(msgText, chatId) {


}

/**
 * Getter for page of every department
 */
function getDepartmentPage(msgText, chatId) {
    //https://uni.lut.fi/fi/web/guest/lukujarjestykset1
    //http://lukkarimaatti.ltky.fi/course/byDepartment/mafy
    /**
    Kauppatieteet - A
    Energiatekniikka - BH
    Kemiantekniikka - BJ
    Kielikeskus - FV
    Konetekniikka - BK
    Laskennallinen  tekniikka - BM
    Sähkötekniikka - BL
    Tietotekniikka - CT
    Tuotantotalous - CS, LM
    Ympäristötekniikka - BH
     */
    if (msgText.substring(0, 1) === 'A') {
        return 'kati'
    } else {
    switch (msgText.substring(0, 2)) {
      case 'BH':
        return 'ente/ymte'
      case 'BJ':
        return 'kete'
      case 'FV':
        return 'kike'
      case 'BK':
        return 'kote'
      case 'BM':
        return 'mafy'
      case 'BL':
        return 'sate'
      case 'CT':
        return 'tite'
      case 'CS':
        return 'tuta'
      case 'LM':
        if (msgText.indexOf('tuta') > 0) {
          return 'tuta'
        } else if (msgText.indexOf('tite') > 0) {
          return 'tite'
        } else if (msgText.indexOf('kati') > 0) {
          return 'kati'
        }
      default:
        console.log('Unknown course code', msgText ? msgText : 'EMPTY')
        return 'UNKNOWN'
    }
  }
}