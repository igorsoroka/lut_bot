## Synopsis
This is Telegram Bot - [https://telegram.org/ ](Link URL).
Telegram is a cloud-based instant messaging service. Telegram clients exist for both mobile (Android, iOS, Windows Phone, Ubuntu Touch) and desktop systems (Windows, OS X, Linux). Users can send messages and exchange photos, videos, stickers and files of any type. Telegram also provides optional end-to-end-encrypted messaging, which has been criticized by security experts because of its custom nature. (from Wikipedia - https://en.wikipedia.org/wiki/Telegram_(software))

### Bot functionality: ###

* Searching through study timetable
* Menus of restaurants at the campus area;
* Find student parties;
* Find exam dates.

## Motivation
It is a trial to create bot helper in order to save time and internet traffic of poor students.

## Installation

Search for : 'lty_bot' in Telegram messenger. But currently the bot is offline, because now I'm searching a platform to deploy it.

## Contributors

If someone wants to help in writing this bot, don't hesitate to contact me - igr.srk@gmail.com